class Consonants:
    vowels = ['a', 'e', 'i', 'o', 'u']
    str = ""

    def __init__(self):
        self.str = ""

    def add_string(self, s):
        if type(s) == int:
            raise Exception("String inputs only")
        self.str = s

    def count_consonants(self):
        count = 0
        for i in self.str:
            if i not in self.vowels:
                count += 1
        return count

    def readFromFile(self, filename):
        infile = open(filename, "r")
        line = infile.readline()
        return line


obj = Consonants()
obj.add_string("demo")
print(obj.count_consonants())
